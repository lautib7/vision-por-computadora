#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
img = cv2.imread('hoja.png', 0)
row=len(img[:,1])
col=len(img[1,:])

for num_row in range(row):
    for num_col in range(col):
        if img[num_row][num_col]<240:
            img[num_row][num_col]=0
        else:
             img[num_row][num_col]=img[num_row][num_col]

cv2.imwrite('resultado.png', img)


