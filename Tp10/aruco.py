import cv2 as cv
import numpy as np
import sys
import math

if (len(sys.argv)>1):
    foto_perf = sys.argv[1]
else:
    print('Ingrese el nombre de la imagen de que contiene la foto del robot')
    sys.exit(0)

img1 = np.zeros((720,980,3),np.uint8)
#imagen para actualizacion de la distancia
img_dist = np.zeros((50,590,3),np.uint8)
img_dist_cop = np.copy(img_dist)
img1 = cv.copyMakeBorder(img1, 10, 10, 10, 10, cv.BORDER_CONSTANT, None, value = [246, 209, 81])

img_foto = cv.imread(foto_perf)
img_foto = cv.resize(img_foto, dsize=(250, 250), interpolation=cv.INTER_CUBIC)
img_foto = cv.copyMakeBorder(img_foto, 2, 2, 2, 2, cv.BORDER_CONSTANT, None, value = [246, 209, 81])

nombre = str(input('Ingrese el nombre del robot: '))
img_nombre = np.zeros((50,180,3),np.uint8)
img_nombre_cop = np.copy(img_nombre)
cv.putText(img_nombre, str(nombre), ((25), (20)), cv.FONT_HERSHEY_COMPLEX, 0.8, (255, 255, 255), 1, cv.LINE_AA)
cv.putText(img1, 'Presione ESC ', ((50), (650)), cv.FONT_HERSHEY_COMPLEX, 0.6, (255, 255, 255), 1, cv.LINE_AA)
cv.putText(img1, 'para salir. ', ((50), (670)), cv.FONT_HERSHEY_COMPLEX, 0.6, (255, 255, 255), 1, cv.LINE_AA)
#ids para deteccion de robot
id1_1 = int(input('Ingrese primer id aruco: '))
id1_2 = int(input('Ingrese segundo id aruco: '))
id1=np.float32([[id1_1],[id1_2]])
img2 = np.zeros((484,644,3),np.uint8)
img2[:,:] = (246, 209, 81)
# Cargamos el diccionario.
dictionary = cv.aruco.Dictionary_get(cv.aruco.DICT_6X6_250)
# Creamos el detector
parameters = cv.aruco.DetectorParameters_create()
cap = cv.VideoCapture(0)
dif = 0

while (1):
    ret ,frame = cap.read()
    row, col, c = frame.shape
    img1[((720-484)-8):(720-8),((980-644)-8):(980-8)] = img2[:,:]
    img_cop = np.copy(img1)
    # Detectamos los marcadores en la imagen
    corners, ids, rejected = cv.aruco.detectMarkers(frame, dictionary, parameters=parameters)
    if(ids is not None):
        if np.any(ids == id1_1) and np.any(ids == id1_2):
            img1 [30:284, 50:304] = img_foto[:, :]
            img1 [320:370, 60:240] = img_nombre[:, :]
            if corners[0][0][0][0] > corners[1][0][0][0]:
                dif =int(corners[0][0][0][0]-corners[1][0][0][0])
            if corners[0][0][0][0] < corners[1][0][0][0]:
                dif = int(corners[1][0][0][0]-corners[0][0][0][0])
            frame = cv.aruco.drawDetectedMarkers(frame, corners, ids)
        else:
            img1 [30:284, 50:304] = np.zeros((254,254,3),np.uint8)
            img1 [320:370, 60:240] = img_nombre_cop[:, :]

    img1[((720-row)-10):(720-10), ((980-col)-10):(980-10)] = frame[:,:]
    #calibracion de la distancia
    if dif>=197:
        img_dist = np.copy(img_dist_cop)
        dist = int(20)
        cv.putText(img_dist, 'Precaucion!!! el robot esta a menos de: ' + str(dist) + 'cm', ((25), (20)), cv.FONT_HERSHEY_COMPLEX, 0.6, (255, 255, 255), 1, cv.LINE_AA)
        img1[125:175, 330:920] = img_dist[:,:]
    if (dif<197) and (dif>=132):
        img_dist = np.copy(img_dist_cop)
        dist = round((20+(98-(dif/2))*math.tan((16.86*math.pi)/180)),2)
        cv.putText(img_dist, 'Precaucion el robot esta a: ' + str(dist) + 'cm', ((25), (20)), cv.FONT_HERSHEY_COMPLEX, 0.6, (255, 255, 255), 1, cv.LINE_AA) 
        img1[125:175, 330:920] = img_dist[:,:]
    if (dif<132) and (dif>=80):
        img_dist = np.copy(img_dist_cop)
        dist = round((20+((66-(dif/2))*math.tan((37.57*math.pi)/180)+10)),2)
        cv.putText(img_dist, 'El robot se encuentra a una distancia de: ' + str(dist) + 'cm', ((25), (20)), cv.FONT_HERSHEY_COMPLEX, 0.6, (255, 255, 255), 1, cv.LINE_AA)
        img1[125:175, 330:920] = img_dist[:,:]
    if (dif<80) and (dif>=57):
        img_dist = np.copy(img_dist_cop)
        dist = round(20+((40-(dif/2))*math.tan((61.19*math.pi)/180)+30),2)
        cv.putText(img_dist, 'El robot se encuentra a una distancia de: ' + str(dist) + 'cm', ((25), (20)), cv.FONT_HERSHEY_COMPLEX, 0.6, (255, 255, 255), 1, cv.LINE_AA)
        img1[125:175, 330:920] = img_dist[:,:]
    if (dif<57) and (dif>=40):
        img_dist = np.copy(img_dist_cop)
        dist = round(30+((28.5-(dif/2))*math.tan((75.07*math.pi)/180)+40),2)
        cv.putText(img_dist, 'El robot se encuentra a una distancia de: ' + str(dist) + 'cm', ((25), (20)), cv.FONT_HERSHEY_COMPLEX, 0.6, (255, 255, 255), 1, cv.LINE_AA)
        img1[125:175, 330:920] = img_dist[:,:]
    if dif<40:
        img_dist = np.copy(img_dist_cop)
        dist = int(100)
        cv.putText(img_dist, 'El robot se encuentra a mas de: ' + str(dist) + 'cm', ((25), (20)), cv.FONT_HERSHEY_COMPLEX, 0.6, (255, 255, 255), 1, cv.LINE_AA)
        img1[125:175, 330:920] = img_dist[:,:]
    if(ids is None):
        img_dist = np.copy(img_dist_cop)
        cv.putText(img_dist, 'No se encuentra ningun robot cercano ', ((25), (20)), cv.FONT_HERSHEY_COMPLEX, 0.6, (255, 255, 255), 1, cv.LINE_AA)
        img1[125:175, 330:920] = img_dist[:,:]
    
    cv.imshow('menu', img1)
    key = cv.waitKey(50)
    if (key == 27):
        break


