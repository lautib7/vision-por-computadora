#! /usr/bin/env python
# -*- coding: utf-8 -*-
#transformacion similaridad, euclidiana y recorte.
import cv2
import numpy as np
import sys
import os
import rotacion_traslacion
import trans_simil
drawing = False
save = True
ix, iy, dx, dy = -1, -1, -1, -1
img_rec, img_recta, x1, y1 = 0, 0, 0, 0
num_row, num_col = 0, 0

def crop_rect(event, x, y, flags, param):
    global ix, iy, drawing, img_rec, dx, dy, x1, y1, img_recta
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True: #genera un nuevo rectangulo por cada movimiento del mouse mientras se presiona el click izquierdo
            img[:] = img_cop[:]
            cv2.rectangle(img, (ix, iy), (x, y), (0, 0, 0), 1)

    elif event == cv2.EVENT_LBUTTONUP:
            drawing = False
            cv2.rectangle(img, (ix, iy), (x, y), (0, 0, 0), 1)
            dx, dy = x, y
            col = abs(x-ix)
            row = abs(y-iy)
            if dx>ix and dy>iy:     #ordena el rectangulo 
                img_rec = img_cop[iy:dy, ix:dx]
            if dx>ix and dy<iy:
                img_rec = img_cop[dy:iy, ix:dx]
            if dx<ix and dy>iy:
                img_rec = img_cop[iy:dy, dx:ix]
            if dx<ix and dy<iy:
                img_rec = img_cop[dy:iy, dx:ix]
if (len(sys.argv)>1):
    img_o = sys.argv[1]
else:
    print('Pasar un nombre de archivo como primer argumento')
    sys.exit(0)
img = cv2.imread(img_o)
cv2.namedWindow( 'image' )
cv2.setMouseCallback( 'image' , crop_rect)
img_cop = np.copy(img)
print('Presione la tecla g para guardar el rectagulo seleccionado')
print('Presione la tecla e para aplicar una transformacion "euclidiana" al rectagulo seleccionado')
print('Presione la tecla s para aplicar una transformacion "similaridad" al rectagulo seleccionado')
print('Presione la tecla r para restaurar la imagen original')
print('Presione la tecla q para salir')

while(1):
    cv2.imshow( 'image' , img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('g'):
        os.system ("clear") #limpia la salida del terminal
        cv2.namedWindow('recorte')  
        cv2.imshow('recorte', img_rec)
        cv2.imwrite('recorte.png', img_rec)
        print('Presione la tecla e para aplicar una transformacion "euclidiana" al rectagulo seleccionado')
        print('Presione la tecla s para aplicar una transformacion "similaridad" al rectagulo seleccionado')
        print('Presione la tecla r para restaurar la imagen original')
        print('Presione la tecla q para salir')
    elif k == ord('r'):
        os.system ("clear")
        img = np.copy(img_cop)
        print('Presione la tecla g para guardar el rectagulo seleccionado')
        print('Presione la tecla e para aplicar una transformacion "euclidiana" al rectagulo seleccionado')
        print('Presione la tecla s para aplicar una transformacion "similaridad" al rectagulo seleccionado')
        print('Presione la tecla q para salir')
    elif k == ord('e'):
        os.system ("clear")
        angle1 = float(input('Ingrese el valor del angulo de rotacion: '))
        (tx1, ty1) = map(int, input('Ingrese los valores de traslacion "x" e "y" separados por un espacio: ').split())
        rotacion_traslacion.rot_eu(angle1, tx1, ty1, img_rec)
        print('Presione la tecla g para guardar el rectagulo seleccionado')
        print('Presione la tecla s para aplicar una transformacion "similaridad" al rectagulo seleccionado')
        print('Presione la tecla r para restaurar la imagen original')
        print('Presione la tecla q para salir')
    elif k == ord('s'):
        os.system ("clear")
        angle2 = float(input('Ingrese el valor del angulo de rotacion: '))
        esc1 = abs(float(input('Ingrese el valor de escala, siendo 1 el tamano original de la imagen: ')))
        (tx2, ty2) = map(int, input('Ingrese los valores de traslacion "x" e "y" separados por un espacio: ').split())
        trans_simil.similaridad(angle2, esc1, tx2, ty2, img_rec)
        print('Presione la tecla g para guardar el rectagulo seleccionado')
        print('Presione la tecla e para aplicar una transformacion "euclidiana" al rectagulo seleccionado')
        print('Presione la tecla r para restaurar la imagen original')
        print('Presione la tecla q para salir')
    elif k == ord('q'):
        break
cv2.destroyAllWindows()
os.system ("clear")
