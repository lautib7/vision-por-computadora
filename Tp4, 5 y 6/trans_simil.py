#! /usr/bin/env python
# -*- cod: utf-8 -*-
#trans_simil.py
import cv2
import numpy as np
import sys
import math
def similaridad(angle, escala, tx, ty, img):
    img_cop = np.copy(img)
    (h, w) = img.shape[:2]
    (x, y) = (w/2, h/2)
    angle_d=((angle*math.pi)/180)
    alfa=math.cos(angle_d)*escala
    beta=math.sin(angle_d)*escala
    rot_x=((1-alfa)*x)-(beta*y)
    rot_y=(beta*x)+((1-alfa)*y)
    M=np.float32(([alfa, beta, (rot_x+tx)],[(-1*beta), alfa, (rot_y+ty)])) #matriz de transformacion
    simil = cv2.warpAffine(img_cop, M, (w, h))
    cv2.imwrite('transformacion_similaridad.png', simil)
    cv2.namedWindow('Similaridad')
    cv2.imshow('Similaridad', simil)
