#! /usr/bin/env python
# -*- coding: utf-8 -*-
#rotacion_traslacion.py
import cv2
import numpy as np
import sys
import math
def rot_eu(angle, tx, ty, img):
    img_cop = np.copy(img)
    (h, w) = img.shape[:2]
    (x, y) = (w/2, h/2)
    angle_d=((angle*math.pi)/180)
    rot_x=((1-math.cos(angle_d))*x)-(math.sin(angle_d)*y)
    rot_y=(math.sin(angle_d)*x)+((1-math.cos(angle_d))*y)
    M=np.float32(([(math.cos(angle_d)),(math.sin(angle_d)),(rot_x+tx)],[(-1*math.sin(angle_d)), (math.cos(angle_d)),(rot_y+ty)])) #matriz de transformacion
    rot_tras = cv2.warpAffine(img_cop, M, (w, h))
    cv2.imwrite('transformacion_euclidiana.png', rot_tras)
    cv2.namedWindow('Euclidiana')
    cv2.imshow('Euclidiana', rot_tras)
