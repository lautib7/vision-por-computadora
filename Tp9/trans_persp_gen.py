#! /usr/bin/env python
# -*- coding: utf-8 -*-
#trans_persp_gen.py
import cv2
import numpy as np
import sys
import math
import os

def general(puntos_g1, puntos_g2, img, cols_g, rows_g):
    global gx, gy, puntos, med, ix, iy, drawing, dis_w_txt, dis_h_txt, x1, y1, cord
    ix, iy = -1, -1
    rgx1, rgy1, rgx2, rgy2, rgx3, rgy3, rgx4, rgy4, puntos = 0, 0, 0, 0, 0, 0, 0, 0, 0
    dis_w_txt, dis_h_txt = 0, 0
    med = True
    drawing = False
    pts1_g = np.float32([[290, 110],[350, 110],[290, 205],[350, 205]])
    col_gen = abs(int(350-290))
    row_gen = abs(int(205-110))
    height = int(28)
    width = int(25)
    if row_gen > height:
        dis_h_txt = row_gen/height
    if row_gen < height:
        dis_h_txt = height/row_gen
    if col_gen > width:
        dis_w_txt = col_gen/width
    if row_gen < width:
        dis_w_txt = width/col_gen


    def ptos(event, x, y, flags, param):
        global ix, iy, puntos, med, drawing, dis_w_txt, dis_h_txt, x1, y1, cord

        if med is False:
            if event == cv2.EVENT_LBUTTONDOWN:
                drawing = True
                ix, iy = x, y
            elif event == cv2.EVENT_MOUSEMOVE:
                if drawing is True:
                    g[:] = img_cop_g[:]
                    cv2.line(g, (ix, iy), (x, y), (20, 255, 57), 1)
            elif event == cv2.EVENT_LBUTTONUP:
                drawing = False
                cv2.line(g, (ix, iy), (x, y), (20, 255, 57), 1)
                r, c, ch = g.shape
                dis_x = abs(ix-x)
                dis_y = abs(iy-y)
                cen_x = int(dis_x/2)
                cen_y = int(dis_y/2)
                if dis_x > dis_y:
                    cv2.line(g, (ix ,(iy+10)), (ix ,(iy-10)), (20, 255, 57), 1)
                    cv2.line(g, (x ,(y+10)), (x ,(y-10)), (20, 255, 57), 1)
                    if (y<50) or (iy<50):
                        if x < ix:
                            x1 = x
                        if ix < x:
                            x1 = ix
                        cv2.putText(g, str(round((dis_x/dis_w_txt),2)) + 'cm', (((cen_x-10)+x1), (y+15)), cv2.FONT_HERSHEY_COMPLEX, 0.6, (255, 255, 255), 1, cv2.LINE_AA)
                    else:
                        if x < ix:
                            x1 = x
                        if ix < x:
                            x1 = ix
                        cv2.putText(g, str(round((dis_x/dis_w_txt),2)) + 'cm', (((cen_x-10)+x1), (y)), cv2.FONT_HERSHEY_COMPLEX, 0.6, (255, 255, 255), 1, cv2.LINE_AA)
                if dis_y > dis_x:
                    cv2.line(g, ((ix+10) ,iy), ((ix-10) ,iy), (20, 255, 57), 1)
                    cv2.line(g, ((x+10) ,y), ((x-10) ,y), (20, 255, 57), 1)
                    if (x>(c-60)) or (ix>(c-60)):
                        if y < iy:
                            y1 = y
                        if iy < y:
                            y1 = iy
                        cv2.putText(g, str(round((dis_y/dis_h_txt),2)) + 'cm', ((x-60), (cen_y+y1)), cv2.FONT_HERSHEY_COMPLEX, 0.6, (255, 255, 255), 1, cv2.LINE_AA)
                    else:
                        if y < iy:
                            y1 = y
                        if iy < y:
                            y1 = iy
                        cv2.putText(g, str(round((dis_y/dis_h_txt),2)) + 'cm', ((x), (cen_y+y1)), cv2.FONT_HERSHEY_COMPLEX, 0.6, (255, 255, 255), 1, cv2.LINE_AA)
                img_cop_g[:] = g[:]
    os.system ("clear")
    M_g = cv2.getPerspectiveTransform(puntos_g1, puntos_g2)
    g = cv2.warpPerspective(img, M_g, (cols_g, rows_g))
    img_cop_g = np.copy(g)
    img_rest_g = np.copy(g)
    cv2.namedWindow('perspectiva_general')
    cv2.setMouseCallback('perspectiva_general', ptos)
    print('Presione m en la imagen para realizar las mediciones')
    print('Presione e en la imagen para cerrarla y regresar a la original')
    
    while(1):
        cv2.imshow('perspectiva_general', g)
        k = cv2.waitKey(1) & 0xFF
        if k == ord('m'):
            med = False
            g[:] = img_rest_g[:]
            img_cop_g[:] = img_rest_g[:]
            os.system ("clear")
            print('Presione m en la imagen para restaurar las mediciones')
            print('Presione e en la imagen para cerrala y regresar a la original')
        elif k == ord('e'):
            os.system ("clear")
            print('Presione h en la imagen original para aplicar una transformacion perspectiva')
            print('Presione q en la imagen original para salir')
            break
    cv2.destroyWindow('perspectiva_general')

