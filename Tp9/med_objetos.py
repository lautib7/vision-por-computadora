#! /usr/bin/env python.
# -*- coding: utf-8 -*-
#med_objetos.py
import cv2
import numpy as np
import sys
import os
import trans_persp_gen
pts_gen = True
rx, ry, ix, iy = -1, -1, -1, -1
puntos = 0
rx1, ry1, rx2, ry2, rx3, ry3, rx4, ry4 = 0, 0, 0, 0, 0, 0, 0, 0
def perspectiva(event, x, y, flags, param):
    global pts, puntos, rx, ry, rgx, rgy, pts_gen
    if event == cv2.EVENT_LBUTTONDOWN:    
        if pts_gen is False:
            rx, ry = x, y
            cv2.circle(img, (rx, ry), 2, (0, 128, 255), -1)
            puntos = puntos+1
            global rx1, ry1, rx2, ry2, rx3, ry3, rx4, ry4, col_g, row_g
            if puntos < 5:
                if puntos == 1:
                    rx1, ry1 = rx, ry
                if puntos == 2:
                    rx2, ry2 = rx, ry
                if puntos == 3:
                    rx3, ry3 = rx, ry
                if puntos == 4:
                    rx4, ry4 = rx, ry
                    pts1_g = np.float32([[rx1, ry1],[rx2, ry2],[rx3, ry3],[rx4, ry4]])
                    col_g = int(((rx2+rx4)/2)-((rx1+rx3)/2))
                    row_g = int(((ry3+ry4)/2)-((ry1+ry2)/2))
                    pts2_g = np.float32([[0, 0], [col_g, 0],[0, row_g], [col_g, row_g]])
                    trans_persp_gen.general(pts1_g, pts2_g, img_cop_g, col_g, row_g) 
                    puntos = 0
                    pts_gen = True
if (len(sys.argv)>1):
    img_o = sys.argv[1]
else:
    print('Pasar un nombre de archivo como primer argumento')
    sys.exit(0)
img = cv2.imread(img_o)

cv2.namedWindow('image', cv2.WINDOW_AUTOSIZE)
row, col, chan = img.shape
if (row > 720) or (col>1280): #ajuste de tamano en caso de ser mayor que la "calidad HD"
    e_row = row/720
    e_col = col/1280
    if e_row > e_col:
        e = e_row
    if e_col > e_row:
        e = e_col
    img = cv2.resize(img, dsize=(int(col/e), int(row/e)), interpolation=cv2.INTER_CUBIC)
cv2.setMouseCallback('image', perspectiva)

img_cop = np.copy(img)
img_cop_g = np.copy(img)
print('Presione h en la imagen original para aplicar una transformacion perspectiva')
print('Presione q en la imagen original para salir')

while(1):
    cv2.imshow( 'image' , img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('r'):
        pts_gen = False
        puntos = 0
        os.system ("clear")
        img = np.copy(img_cop)
        print('Elija en la imagen los puntos para la trasformacion perspectiva')
        print('Presione r para restaurar la imagen original')
        print('Presione q en la imagen para salir')
    elif k == ord('h'):
        pts_gen = False
        os.system ("clear")
        print('Elija en la imagen los puntos para la trasformacion perspectiva')
        print('Presione r para restaurar la imagen original')
        print('Presione q en la imagen para salir')
    elif k == ord('q'):
        os.system ("clear")
        break
cv2.destroyAllWindows()

