#! /usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import cv2 as cv
MIN_MATCH_COUNT = 10
img1 = cv.imread('mundo1.jpg')
img1 = cv.resize(img1, dsize=(1080, 720), interpolation=cv.INTER_CUBIC)
img2 = cv.imread('mundo2.jpg')
img2 = cv.resize(img2, dsize=(1080, 720), interpolation=cv.INTER_CUBIC)
rows, cols, c = img1.shape
sift = cv.SIFT.create()
kp1, des1 = sift.detectAndCompute(img1, None)
kp2, des2 = sift.detectAndCompute(img2, None)
matcher = cv.BFMatcher(cv.NORM_L2)
matches = matcher.knnMatch(des1, des2, k=2)
# Guardamos los buenos matches usando el test de razón de Lowe
good = []
for m, n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)
if (len(good) > MIN_MATCH_COUNT):
    src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
    H, mask = cv.findHomography(dst_pts, src_pts, cv.RANSAC, 5.0) # Computamos la homografía con RAN-SAC
wimg2 = cv.warpPerspective(img2, H, (cols, rows), flags=cv.INTER_LINEAR)
# Mezclamos ambas imágenes
alpha = 0.5
print('Presione esc para salir')

while(1):
    blend = np.array(wimg2*alpha+img1*(1-alpha), dtype=np.uint8)
    cv.imshow('Mundo1', img1)
    cv.namedWindow('Mundo1', cv.WINDOW_NORMAL)
    cv.imshow('Mundo2', img2)
    cv.namedWindow('Mundo2', cv.WINDOW_NORMAL)
    cv.imshow('Blend', blend)
    cv.namedWindow('Blend', cv.WINDOW_NORMAL)
    k = cv.waitKey(1) & 0xFF
    if k == 27:
        break
cv.destroyAllWindows()
