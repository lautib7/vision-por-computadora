#! /usr/bin/env python
# -*- coding: utf-8 -*-
#trans_persp.py
import cv2
import numpy as np
import sys
import math
def persp(puntos1, puntos2, img):
    rows, cols, ch = img.shape
    M = cv2.getPerspectiveTransform(puntos1, puntos2)
    p = cv2.warpPerspective(img, M, (cols, rows))
    cv2.namedWindow('Transformacion_perspectiva')
    cv2.imshow('Transformacion_perspectiva', p)

