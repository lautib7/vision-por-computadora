#! /usr/bin/env python
# -*- coding: utf-8 -*-
#trans_persp_b.py
import cv2
import numpy as np
import sys
import os
import trans_persp
import trans_afin

pts = True
pts_pers = True
rx, ry = -1, -1
puntos = 0
rx1, rx2, rx3 = 0, 0, 0
rp1, rp2, rp3, rp4 = 0, 0, 0, 0

def afinidad(event, x, y, flags, param):
    global pts, puntos, rx, ry
    if event == cv2.EVENT_LBUTTONDOWN:
        if pts is False:
            rx, ry = x, y
            cv2.circle(img, (rx, ry), 1, (0, 0, 0), 1)
            puntos = puntos+1
            global rx1, rx2, rx3
            if puntos < 4:
                if puntos == 1:
                    rx1 = rx, ry
                if puntos == 2:
                    rx2 = rx, ry
                if puntos == 3:
                    rx3 = rx, ry
                    pts1 = np.float32([[rx1],[rx2],[rx3]])
                    trans_afin.afin(pts1, img, imgb)
                    puntos = 0
        if pts_pers is False:
            rx, ry = x, y
            r,c,ch = img_cop.shape
            cv2.circle(img, (rx, ry), 1, (0, 0, 0), 1)
            puntos = puntos+1
            global rp1, rp2, rp3, rp4
            if puntos < 5:
                if puntos == 1:
                    rp1 = rx, ry
                if puntos == 2:
                    rp2 = rx, ry
                if puntos == 3:
                    rp3 = rx, ry
                if puntos == 4:
                    rp4 = rx, ry
                    pts1_p = np.float32([[rp1],[rp2],[rp3],[rp4]])
                    pts2_p = np.float32([[0,0],[c,0],[0,r],[c,r]])
                    trans_persp.persp(pts1_p, pts2_p, img_copa)
                    puntos = 0


if (len(sys.argv)>2):
    img_o = sys.argv[1]
    img_b = sys.argv[2]
else:
    print('Pasar un nombre de archivo como primer argumento')
    sys.exit(0)
img = cv2.imread(img_o)
imgb = cv2.imread(img_b)
cv2.namedWindow( 'image' )
cv2.setMouseCallback( 'image' , afinidad)
img_copa = np.copy(img)
img_cop = np.copy(img)
print('Presione la tecla a para aplicar una transformacion afinidad')
print('Presione la tecla h para aplicar una transformacion perspectiva')

while(1):
    cv2.imshow( 'image' , img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('a'):
        pts_pers = True
        puntos = 0
        pts = not pts
        os.system ("clear")
        print('Elija en la imagen los puntos para la trasformacion afinidad')
        print('Presione la tecla r para restaurar la imagen')
        print('Presione la tecla h para aplicar una transformacion perspectiva')
    elif k == ord('r'):
        puntos = 0
        os.system ("clear")
        img = np.copy(img_cop)
        if pts is True:
            print('Elija en la imagen los puntos para la trasformacion perspectiva')
            print('Presione la tecla r para restaurar la imagen')
            print('Presione la tecla a para aplicar una transformacion afinidad')
        if pts_pers is True:
            print('Elija en la imagen los puntos para la trasformacion afinidad')
            print('Presione la tecla r para restaurar la imagen')
            print('Presione la tecla h para aplicar una transformacion perspectiva')

    elif k == ord('h'):
        pts = True
        pts_pers = not pts_pers
        puntos = 0
        os.system ("clear")
        print('Elija en la imagen los puntos para la trasformacion perspectiva')
        print('Presione la tecla r para restaurar la imagen')
        print('Presione la tecla a para aplicar una transformacion afinidad')
    elif k == ord('q'):
        break
cv2.destroyAllWindows()

