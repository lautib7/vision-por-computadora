#! /usr/bin/env python
# -*- coding: utf-8 -*-
#trans_afin.py
import cv2
import numpy as np
import sys
import math
img_a = 0
def afin(puntos2, img, img2): 
    global img_a
    rows, cols, ch = img.shape
    rows2, cols2, ch2 = img2.shape
    puntos1 = np.float32([[0,rows2],[0,0],[cols2,0]])
    M = cv2.getAffineTransform(puntos1, puntos2)
    a = cv2.warpAffine(img2, M,(cols, rows))
    rows1, cols1, ch1 = a.shape
    mask = np.ones((rows1, cols1),dtype=np.uint8)
    img_a = cv2.bitwise_and(a, a, mask = mask)  #mascara a la imagen obtenida de la transformacion

    row=len(a[:,1])
    col=len(a[1,:])
    for num_row in range(row):  #incrustacion de imagen aplicando transformacion afin
        for num_col in range(col):
            if (a[num_row, num_col, 0]!=(0)) and (a[num_row, num_col, 1]!=(0)) and ([num_row, num_col, 2]!=(0)):
                img[num_row][num_col]=a[num_row][num_col]


